import { StateNavigator } from "navigation";
import { NavigationHandler } from "navigation-react";
import { NavigationStack } from "navigation-react-native";
import React from "react";

import { Home } from "./Screens/Home";


interface Props
{
}

interface States
{
}

export class Router extends React.Component<Props, States>
{
    private _addModalRouterNavigator: StateNavigator = new StateNavigator([
        {
            key: "Home",
            renderScene: (): any => <Home router={this._addModalRouterNavigator} />
        }
    ]);

    constructor(props: any)
    {
        super(props);
    }

    public componentDidMount(): void
    {
        this._addModalRouterNavigator.navigate("Home");
    }

    public render(): JSX.Element
    {
        return (
            <NavigationHandler stateNavigator={this._addModalRouterNavigator}>
                <NavigationStack />
            </NavigationHandler>
        );
    }
}