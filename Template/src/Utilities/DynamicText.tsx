import React from "react";
import { PlatformColor, Text } from "react-native";

interface Props
{
    color?: string;
    children?: any;
    style?: any;
    numberOfLines?: number;
}

export class DynamicText extends React.Component<Props, {}>
{
    public render(): any
    {
        const dynamicColor: any = PlatformColor("label", "@android:color/black");

        return (<Text style={{color: dynamicColor, ...this.props?.style}} numberOfLines={this.props?.numberOfLines} >{this.props.children}</Text>);
    }
}