import { StateNavigator } from "navigation";
import { NavigationBar } from "navigation-react-native";
import React from "react";
import { PlatformColor, ScrollView, StyleSheet } from "react-native";

import { DynamicText } from "../Utilities/DynamicText";



interface Props
{
    router?: StateNavigator;
}

interface States
{
}


export class Home extends React.Component<Props, States>
{

    constructor(props: any)
    {
        super(props);

        this.state = {
        };
    }

    public async componentDidMount(): Promise<void>
    {
        // Do something when component mounts
    }

    public componentWillUnmount(): void
    {
        // Do something when component will unmount
    }


    public render(): React.ReactNode
    {
        const styles: any = StyleSheet.create({
            Background: {
                backgroundColor: PlatformColor("systemBackground", "@android:color/background_light")
            },
            Text: {
                marginTop: 150,
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 20,
                opacity: 0.5
            }
        });

        return (
            <ScrollView contentInsetAdjustmentBehavior={"always"} style={styles.Background} >
                <NavigationBar title="Home" largeTitle={true} />
                <DynamicText style={styles.Text}>Hello, World</DynamicText>
            </ScrollView>
        );
    }
}